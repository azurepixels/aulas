<?php

class Cliente extends CI_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('test');
        $this->load->view('common/footer');
      
    }
    public function prontuario(){
        echo 'Esta é a página que exibe os dados dos clientes';
    }

    public function cadastro($card, $jumbo){
        
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['formulario'] = $this->load->view('aula2/form_cadastro', '', true);

        $this->load->model('CardModel');
        $data = $this->CardModel->card_data($card);
        $v['cartao'] = $this->load->view('aula2/card', $data, true); 

        $this->load->model('JumbotronModel');
        $data = $this->JumbotronModel->jumbo_data($jumbo);
        $v['jumbotron'] = $this->load->view('aula2/jumbotron', $data, true); 

        $v['image'] = $this->load->view('aula2/image', '', true);   
        $this->load->view('aula2/layout', $v);
        $this->load->view('common/footer');
    }

    public function editar(){
        echo 'Editar dados do cliente';
    }
    
}

?>