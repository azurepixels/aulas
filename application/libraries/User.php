<?php

class User{
    //Obrigatórias
    private $nome;
    private $sobrenome;
    private $senha;
    private $email;
    
    //opcionais
    private $telefone;

    private $db;

    function __construct($nome=null, $sobrenome=null, $email=null, $senha=null){
        $this->nome = $nome;
        $this->email= $email;
        $this->senha = $senha;
        $this->sobrenome = $sobrenome;

        //acesso ao banco de dados pelo codeigniter
        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }

    public function save(){
        $sql = "INSERT INTO user (nome, email, senha, sobrenome, telefone) 
        VALUES ('$this->nome', '$this->email', '$this->senha', '$this->sobrenome', '$this->telefone')"; 
        $this->db->query($sql);
    }
    /**
     * Obtém a lista de todos os usuários cadastrados=null
     * @return associative_array
     */
    public function getALL(){
        $sql = "SELECT * FROM user";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    public function getById($id){
        $rs = $this->db->get_where('user', "id = $id");
       // return $rs->result_array()[0];
       return $rs->row_array();
    }

    public function update($data, $id){
        $this->db->update('user', $data, "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('user', "id = $id");
        
    }

}

?>