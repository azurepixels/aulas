<?php
include_once 'DAO.php';

class Endereco extends DAO {
    
    function __construct(){
        parent::__construct('endereco');
    }

    // sobrescrita de método
    public function salvar($data, $id_pessoa = 0){
        $data['id_pessoa'] = $id_pessoa;
        parent::salvar($data);
    }
}


?>