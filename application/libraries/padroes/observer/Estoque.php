<?php
include_once 'VendasListener.php';

class Estoque implements VendasListener{
    private $data;

    function __construct($data){
        $this->data = $data;
    }

    public function venda_realizada($produto){
        $dp = $produto->departamento;
        $nome = $produto->nome;
        $this->solicita_reposicao($nome, $dp);
    }

    private function solicita_reposicao($nome, $dp){
        echo "Vendido o produto $nome do departamento $dp<br>";
        echo "Gerar solicitação de compra/reposição";
    }

    public function compra_realizada($produto){
        
    }
}


?>