<?php

class Loja{
    private $listenerList;
    
    public function addListener(VendasListener $listener){
        $this->listenerList[] = $listener;
    }

    public function removeListener(VendasListener $listener){
        foreach ($this->listenerList as $key => $obj) {
            if($obj == $listener){
                unset($this->listenerList[$key]);
            }
        }
    }

    private function notify($method, $produto){
        foreach ($this->listenerList as $obj) {
            $obj->{$method}($produto);
        }
    }

    public function realiza_venda(){
        $prod = new stdClass;
        $prod->nome = 'Mini System Aywa';
        $prod->preco = 1853.78;
        $prod->id_usuario = 2;
        $prod->departamento = 'Eletro-Eletrônicos';

        $this->notify('venda_realizada', $prod);
    }

}


?>