<?php

abstract class Mensagem{
    protected $listaUsuarios;
    
    function __construct(array $listaUsuarios){
        $this->listaUsuarios = $listaUsuarios;
    }

    // método de template
    public function venda_realizada($produto){
        $id = $produto->id_usuario;
        $usuario = $this->listaUsuarios[$id];

        //template: método que deve ser definido nas classes filhas
        $this->envia($usuario);
    }

    protected abstract function envia($usuario);
}


?>