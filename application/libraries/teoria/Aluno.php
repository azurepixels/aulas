<?php

include_once 'Pessoa.php';

// leia como: aluno é pessoa
class Aluno extends Pessoa{
    private $turma;
    private $curso;

    function __construct($nome, $idade, $turma){
        parent::__construct($nome,$idade);
        $this->turma = $turma;
    }

    public function setTurma($turma){
        $this->turma = $turma;
    }

    public function getTurma(){
        return $this->turma;
    }

    public function estudar(){
        echo 'Passar horas fazendo altas gameplays<br />';
    }

    public function pesquisar(){
        echo 'Procurar uma skill foda pro seu personagem<br />';
    }

    public function limiteLivros(){
        return 7;
    }

    public function prazoDeEntrega(){
        return 21;
    }

    public function atraso(){
        return 10;
    }
}