<?php

class Biblioteca{
    //aplicação de polimorfismo usando a classe Pessoa
    public function emprestimo(array $livros, Pessoa $usuario){
        if($usuario->limiteLivros() >= count($livros) ){
            echo "<br />Limite aceito: Gerar ticket de empréstimo para: ".$usuario->getNome();
        }
        else echo "<br />Usuário excedeu a quantidade de livros permitida";
    }

    public function devolucao(array $livros, Pessoa $usuario){
        $dias = $usuario->atraso();

        $m0[0] = 0;
        $m1[1] = 1;
        $m2_5 = array_fill(2, 4, 1.5);
        $m6_10 = array_fill(6, 5, 2.5);
        $m11[11] = 4;

        $multa = array_merge($m0, $m1, $m2_5, $m6_10, $m11);
        for ($i=0; $i <= $dias && $i <= 11 ; $i++) { 
            $valor = $multa[$i];
        }
        if($dias != 0){
            echo "<br />Livros atrasados. Multa de R$".$valor * count($livros)." para: ".$usuario->getNome();
        }
        else echo "<br />Nenhum livro atrasado.";
    }
}


?>