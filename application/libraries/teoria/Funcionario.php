<?php
include_once "Pessoa.php";

class Funcionario extends Pessoa{
    private $setor;
    private $funcao;
    
    function __construct($nome, $idade, $funcao){
        parent::__construct($nome, $idade);
        $this->funcao = $funcao;
    }

    public function trabalhar(){
        echo 'Rodar bolsa depois da meia noite<br />';
    }
    
    public function limiteLivros(){
        return 4;
    }

    public function prazoDeEntrega(){
        return 14;
    }

    public function atraso(){
        return 0;
    }
}


?>