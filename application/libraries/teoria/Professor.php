<?php

include_once 'Pessoa.php';

class Professor extends Pessoa{
    private $escola;
    private $curso = array();

    function __construct($nome, $idade, $escola){
        parent::__construct($nome, $idade);
        $this->escola = $escola;
    }

    public function addCurso($curso){
        $this->curso[] = $curso;
    }

    public function lecionar(){
        echo 'Ler uns slides que ninguém se importa<br />';
    }

    public function pesquisar(){
        echo 'Procurar como usa o Moodle<br />';
    }
    
    public function limiteLivros(){
        return 10;
    }

    public function prazoDeEntrega(){
        return 30;
    }

    public function atraso(){
        return 2;
    }
}

?>