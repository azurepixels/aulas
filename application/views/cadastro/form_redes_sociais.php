<div class="container mt-3">


    <div class="card">
        <div class="card-header"><h4>Redes Sociais</h4></div>
        <div class="card-body">
                

    <div class="form-row">

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" value="<?=set_value('facebook') ?>" class="form-control" name="facebook" id="facebook" placeholder="">
            <label for="Facebook">Facebook</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text"  value="<?=set_value('twitter') ?> "class="form-control" name="twitter" id="twitter" placeholder="">
            <label for="twitter">Twitter</label>
        </div>
        </div>

    </div>

    <div class="form-row">

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" value="<?=set_value('instagram') ?>" class="form-control" name="instagram" id="instagram" placeholder="">
            <label for="instagram">Instagram</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text"  value="<?=set_value('linkedin') ?> "class="form-control" name="linkedin" id="linkedin" placeholder="">
            <label for="linkedin">Linkedin</label>
        </div>
        </div>

    </div>


    <button class="btn btn-info my-4 btn-block" type="submit">Cadastrar</button>
    </form>

    </div>
    </div>

</div>