
--
-- Database: `lp6`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` int(11) NOT NULL,
  `id_pessoa` int(11) NOT NULL,
  `tipo_logradouro` varchar(15) NOT NULL,
  `nome_logradouro` varchar(50) NOT NULL,
  `numero` smallint(6) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `cep` varchar(10) NOT NULL,
  `cidade` varchar(28) NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;